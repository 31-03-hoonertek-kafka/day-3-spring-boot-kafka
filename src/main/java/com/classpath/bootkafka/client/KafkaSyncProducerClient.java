package com.classpath.bootkafka.client;

import com.classpath.bootkafka.model.Order;
import org.springframework.boot.CommandLineRunner;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class KafkaSyncProducerClient  implements CommandLineRunner {
    private final KafkaTemplate<String, Order> kafkaTemplate;

    public KafkaSyncProducerClient(KafkaTemplate<String, Order> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }


    @Override
    public void run(String... args) throws Exception {
    /*    System.out.println(" =============== About to send message to Kafka ==============");
        SendResult<String, Order> response = this.kafkaTemplate.send("new-orders",
                                                                          Order
                                                                                  .builder()
                                                                                    .id(1111L)
                                                                                    .customerName("Ravish")
                                                                                    .price(2000).
                                                                                  build()).get(3, TimeUnit.SECONDS);
        System.out.println("Response from the broker:: ");
        System.out.println("Offset :: "+response.getRecordMetadata().offset());
        System.out.println("Partition :: "+response.getRecordMetadata().partition());
        System.out.println("Topic :: "+response.getRecordMetadata().topic());
*/
    }
}
