package com.classpath.bootkafka.client;

import com.classpath.bootkafka.model.Order;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.boot.CommandLineRunner;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class KafkaMessageProducer implements CommandLineRunner {

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public void run(String... args) throws Exception {
         Faker faker = new Faker();
         while (true){
             Thread.sleep(4000);
             SendResult<String, String> metaData =
                     this.kafkaTemplate.send("pradeep-facts-topic", faker.chuckNorris().fact().toUpperCase()).get(2, TimeUnit.SECONDS);
             RecordMetadata recordMetadata = metaData.getRecordMetadata();
             int partition = recordMetadata.partition();
             long offset = recordMetadata.offset();
             System.out.println("==========================================================");
             System.out.println(" Partition number "+ partition + " with offset:: "+ offset);
             System.out.println("==========================================================");

         }
    }
}
