package com.classpath.bootkafka.config;

import com.classpath.bootkafka.model.Order;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaProducerConfig {

    @Bean
    public Map<String, Object> producerConfigs(){
        HashMap<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "139.59.64.198:9092, 128.199.16.50:9092,157.245.98.120:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.ACKS_CONFIG, "1");
        props.put(ProducerConfig.RETRIES_CONFIG , 2);
        return props;
    }

    @Bean
    public ProducerFactory<String, Order> orderProducerFactory(){
        Map<String, Object> producerConfigs = producerConfigs();
        producerConfigs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(producerConfigs);
    }
    @Bean
    public ProducerFactory<String, String> factProducerFactory(){
        Map<String, Object> producerConfigs = producerConfigs();
        producerConfigs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new DefaultKafkaProducerFactory<>(producerConfigs);
    }

    @Bean
    public KafkaTemplate<String,Order> kafkaTemplateForOrder(){
        return new KafkaTemplate<>(orderProducerFactory());
    }
    @Bean
    public KafkaTemplate<String,String> kafkaTemplateWithFact(){
        KafkaTemplate<String, String> stringStringKafkaTemplate = new KafkaTemplate<>(factProducerFactory());
        return stringStringKafkaTemplate;
    }
}
